public with sharing class fdtn_AccountController {
    
    @AuraEnabled(cacheable=true)
    public static List<Account> getAccountAll(){
        List<Account> accountList = [SELECT Name, Type, CreatedDate from Account LIMIT 15];
        return accountList;
    }
    
    @AuraEnabled(cacheable=true)
    public static fdtn_PaginatorEntity.fdtn_PaginatorResponse pageRecords(Integer currentPage, Integer pageSize){
        /* String queryString = 'SELECT Name, Type, CreatedDate from Account order by Name asc';
        String countString = 'SELECT COUNT() FROM Account';
        fdtn_SOQL fdtnSoql = new fdtn_SOQL(queryString,countString); */
        fdtn_SOQL fdtnSoql = new fdtn_SOQL.Builder('Account')
                                .addField('Name, Type, CreatedDate')
                                .orderBy(new fdtn_SOQL.SortOrder('Name', fdtn_SOQL.SortDirection.ASCENDING))
                                .build();
        
        return new fdtn_PaginatorHelper(fdtnSoql, pageSize, currentPage).getPaginatorInfo();
    }

    @AuraEnabled(cacheable=true)
    public static List<CustomAccount> getAllTreeData(){
        List<Account> accountList = [SELECT Id, Name, ParentId FROM Account order by ParentId];

        List<CustomAccount> customAccList = new List<CustomAccount>();
        List<CustomAccount> rootList = new List<CustomAccount>();

        for(Account acc : accountList){
            CustomAccount customAcc = new CustomAccount();
            customAcc.Id = acc.Id;
            customAcc.text = acc.Name;
            customAcc.parentId = acc.ParentId;
            customAcc.children = new List<CustomAccount>();
            customAccList.add(customAcc);
            if(acc.ParentId == null){
                rootList.add(customAcc);
            }
        }

        for(CustomAccount node : rootList){
            getTreeData(node, customAccList);
            
        }
		system.debug('rootList:' + rootList);
        return rootList;
    }


    public static void getTreeData(CustomAccount parentObj, List<CustomAccount> customAccList){
        List<CustomAccount> nodeList = new List<CustomAccount>();
        for(CustomAccount customAcc : customAccList){
            if(customAcc.parentId != null && customAcc.parentId.equals(parentObj.Id)){
                getTreeData(customAcc, customAccList);
                
                nodeList.add(customAcc);
                parentObj.children.addAll(nodeList);
            }
        }
    }



    public class CustomAccount {
        @AuraEnabled
        public String Id {get; set;}
        @AuraEnabled
        public String parentId {get; set;}
        @AuraEnabled
        public String text {get; set;}
        @AuraEnabled    
        public List<CustomAccount> children;
    }

}