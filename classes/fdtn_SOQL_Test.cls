@isTest
private  class fdtn_SOQL_Test {
    public class TestException extends Exception {}
    
    @testSetup
    static void setData(){
        Account acc = new Account();
        acc.Name = 'Collin Feng';
        acc.NumberOfEmployees = 12345678;
        insert acc;
    }
    
    @isTest
    static void testBuilder(){
        Test.startTest();
        String queryString = 'SELECT Name, Type, CreatedDate  FROM Account  WHERE (Name  = \'Collin Feng\' AND Name  != \'Feng\' AND NumberOfEmployees  < 123456789 AND NumberOfEmployees  <= 12345678 AND NumberOfEmployees  > 123456 AND Name  LIKE \'%Feng%\')  ORDER BY Name  ASC  LIMIT 1  OFFSET 0';
        String countString = 'SELECT COUNT()  FROM Account  WHERE (Name  = \'Collin Feng\' AND Name  != \'Feng\' AND NumberOfEmployees  < 123456789 AND NumberOfEmployees  <= 12345678 AND NumberOfEmployees  > 123456 AND Name  LIKE \'%Feng%\')';
        fdtn_SOQL fdtnSoqlOriginal = new fdtn_SOQL(queryString,countString);
        fdtn_SOQL fdtnSoql = new fdtn_SOQL.Builder('Account')
                                .addField('Name, Type, CreatedDate')
            					.whereCondition(new fdtn_SOQL.Condition().equals('Name', 'Collin Feng')
                                                .notEquals('Name', 'Feng').lessThan('NumberOfEmployees', 123456789)
                                                .lessOrEqual('NumberOfEmployees', 12345678)
                                                .greaterThan('NumberOfEmployees', 123456)
                                                .isLike('Name', '%Feng%'))
            					.offset(0)
            					.limits(1)
                                .orderBy(new fdtn_SOQL.SortOrder('Name', fdtn_SOQL.SortDirection.ASCENDING))
                                .build();
       
        Account accountObj = (Account)fdtnSoql.getRecord();
        system.debug('accountObj: ' + accountObj);
        List<Account> accountObjList = (List<Account>)fdtnSoql.getRecords();
        
        Integer recordCount = fdtnSoql.getRecordCount();
        
        System.assertEquals(queryString, fdtnSoqlOriginal.getQueryString());
        System.assertEquals(countString, fdtnSoqlOriginal.getCountString());
        System.assertEquals(queryString, fdtnSoql.getQueryString());
        System.assertEquals(countString, fdtnSoql.getCountString());
        /**System.assertEquals('Collin Feng', accountObj.Name);
        System.assertEquals(1, accountObjList.size());
        System.assertEquals(1, recordCount);*/
        Test.stopTest();
    }

}