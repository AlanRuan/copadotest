@isTest
public without sharing class RSAEncryptionOwnPrivateKeyTest {
    @isTest
    public static void Test()
    {
        //generate private key
        //openssl command: openssl genrsa -des3 -out private.pem 2048
        
        //openssl command: openssl rsa -in private.pem -noout -text
        //public key modulus (remove colons, remove 00 prefix & make into single line)
        //public key exp 0x10001 = 10001
        RSAEncyption.PublicKey pub = new RSAEncyption.PublicKey('b38189636109fbfa91c3386d9050d4098b54e244c02b4503c27ad74bad3e6edfabdf87aa5099ff9e97721015d9199ae14cdb0256278ec3ffcd55fb6e96a03749df0453df82ffe29dff35363fb8f39bdabe74e03007e86192b3796188cea4ebc84fa84cbe854fb0b92f0656d1aa3fa39cc77737716bc9132ade90311acf1d5e045f1ad90ef8ebaaf936bd6dab211efb7ecbfece5bd25a578d1245055caef404c96c8b3b8b015718e4e12f29cddead1fefd8cc00bd67116d88ba3de4229d5840825d0694af4ec22579a31addd6a328a33c6c3a89aaa42d13af363f15a17a53a18501bdb1846e1b98bfc26431b9e7a5fb1e465814e6d090eb13856f57d49faed04b'                                       
                                              ,'10001');    
                                              
        RSAEncyption r = new RSAEncyption(pub);
        
        String message = 'mrt=12000771&source=c360';
        String encrypted = r.encrypt(message);
        
        System.debug('Original Message: ' + message);
        System.debug('Encrypted Message: ' + encrypted);
        
        //openssl command: openssl rsa -in private.pem -noout -text
        //private key modulus (remove colons, remove 00 prefix & make into single line), same value as public key mod
        //private key privateExponent (remove colons & make into single line)
        RSAEncyption.PrivateKey pvt = new RSAEncyption.PrivateKey('b38189636109fbfa91c3386d9050d4098b54e244c02b4503c27ad74bad3e6edfabdf87aa5099ff9e97721015d9199ae14cdb0256278ec3ffcd55fb6e96a03749df0453df82ffe29dff35363fb8f39bdabe74e03007e86192b3796188cea4ebc84fa84cbe854fb0b92f0656d1aa3fa39cc77737716bc9132ade90311acf1d5e045f1ad90ef8ebaaf936bd6dab211efb7ecbfece5bd25a578d1245055caef404c96c8b3b8b015718e4e12f29cddead1fefd8cc00bd67116d88ba3de4229d5840825d0694af4ec22579a31addd6a328a33c6c3a89aaa42d13af363f15a17a53a18501bdb1846e1b98bfc26431b9e7a5fb1e465814e6d090eb13856f57d49faed04b'
                                               ,'008b1c77419a0167d2cec5b0e44ff423eee598b9e5b7db9097924f2aac4e2d553567f84a25f662f21390a067d0d6b79e2cbe27f80e96877063bc58c70af41663e28ebfc6d42fac688e332aa4c90b675740555b342664ea753b80752fde1aeefa7ef9249f9d02a93876baf7b9aeb0b6b2f01c30cab9ce633cc47b48d995422f2b1d00d5a8aa2e2627fc84cef67b933bd0601116f7a2c7ad51a586c6f61896a8d17d983842a5db26b8ba8552c7f930735ca7b9fbeeaca76b9a2f65a859004300aa4e7699a4872e30e2e2f8363af86fb2b3c038080f150d4b2629243e28d033f1ba180bedd7bd5321f6e5e57648111805ddd25920b687d35cbfd084b9ec1b32c34341');

        RSAEncyption r2 = new RSAEncyption(pvt);
        
        try
        {    
            String decrypted = r2.decrypt(encrypted );
            System.debug('Decrypted Message: ' + decrypted);
            system.assertequals(message,decrypted);
        }
        catch(Exception ex)
        {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString()); 
        }
    }
}