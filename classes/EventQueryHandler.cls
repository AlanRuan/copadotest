/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-12-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class EventQueryHandler {
    public static List<ReportEvent> getReportEvents() {
        return [SELECT ColumnHeaders,EventDate,ExportFileFormat,Format,NumberOfColumns,QueriedEntities,Records,ReportId,RowsProcessed,UserId,Username FROM ReportEvent where EventDate = Today];
        }
    }