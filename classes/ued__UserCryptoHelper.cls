/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UserCryptoHelper {
    global UserCryptoHelper() {

    }
    global static String doDecrypt(String encryptedToken) {
        return null;
    }
    global static String doDecryptWithUrlDecoding(String encryptedToken, String encodingScheme) {
        return null;
    }
    global static String doEncrypt(String plainToken) {
        return null;
    }
    global static String doEncrypt(String plainToken, Id userId) {
        return null;
    }
    global static String doEncryptWithUrlEncoding(String plainToken, String encodingScheme) {
        return null;
    }
    global static String doEncryptWithUrlEncoding(String plainToken, Id userId, String encodingScheme) {
        return null;
    }
global virtual class UserEncryptionDecryptionException extends Exception {
}
}
