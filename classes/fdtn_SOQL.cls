public inherited sharing class fdtn_SOQL {
    
    private String queryString;
    private String countString;
    
    public fdtn_SOQL() {
    }

    public fdtn_SOQL(String queryString) {
        this.queryString = queryString;
    }

    public fdtn_SOQL(String queryString, String countString) {
        this(queryString);
        this.countString = countString;
    }

    public String getQueryString() {
        return this.queryString;
    }

    public String getCountString() {
        return this.countString;
    }

    public sObject getRecord() {
        return Database.query(this.queryString);
    }

    public List<sObject> getRecords() {
        return Database.query(this.queryString);
    }

    public Integer getRecordCount(){
        return Database.countQuery(this.countString);
    }

    public enum LogicalOperator {
        AND_VALUE,
        OR_VALUE
    }

    public enum SortDirection {
        ASCENDING,
        DESCENDING
    }


    public class Builder {
        public String sObjectType;
        private Set<String> fieldsToSelect;
        private Condition whereCondition;
        private List<SortOrder> sortOrder;
        private Integer limits;
        private Integer offset;

        public Builder(String sObjectType){
            this.sObjectType = sObjectType;
            this.fieldsToSelect = new Set<String>();
            this.whereCondition = new Condition();
            this.sortOrder = new List<SortOrder>();
        }

        public fdtn_SOQL build(){
            List<String> queryStatements = new List<String>();
            List<String> countStatements = new List<String>();
            countStatements.add('SELECT COUNT() ');
            addSelectStatement(queryStatements);
            addFromStatement(queryStatements, countStatements);
            addWhereStatement(queryStatements, countStatements);
            addOrderByStatement(queryStatements);
            addLimitStatement(queryStatements);
            addOffsetStatement(queryStatements);

            return new fdtn_SOQL(String.join(queryStatements, ' '), String.join(countStatements, ' '));
        }

        private void addSelectStatement(List<String> queryStatements){
            List<String> fieldList = new List<String>(this.fieldsToSelect);
            if(fieldList.isEmpty()){
                fieldList.add('Id');
            }

            queryStatements.add('SELECT ' + String.join(fieldList, ', '));
        }

        private void addFromStatement(List<String> queryStatements, List<String> countStatements){
            queryStatements.add(' FROM ' + this.sObjectType);
            countStatements.add('FROM ' + this.sObjectType);
        }

        private void addWhereStatement(List<String> queryStatements, List<String> countStatements){
            String whereCondString = this.whereCondition.toConditionString();
            if(String.isNotBlank(whereCondString)){
                queryStatements.add(' WHERE ' + whereCondString);
                countStatements.add(' WHERE ' + whereCondString);
            }
        }

        private void addOrderByStatement(List<String> queryStatements){
            List<String> sortOrderStrings = new List<String>();
            for( SortOrder so : this.sortOrder){
                sortOrderStrings.add(so.toSortOrderString());
            }
            if(!sortOrderStrings.isEmpty()){
                queryStatements.add(' ORDER BY ' + String.join(sortOrderStrings, ', '));
            }
        }

        private void addLimitStatement(List<String> queryStatements){
            if(this.limits != null){
                queryStatements.add(' LIMIT ' + this.limits);
            }
        }

        private void addOffsetStatement(List<String> queryStatements){
            if(this.offset != null){
                queryStatements.add(' OFFSET ' + this.offset);
            }
        }

        public Builder addField(String field){
            fieldsToSelect.add(field);
            return this;
        }

        public Builder addFieldList(List<String> fieldsList){
            fieldsToSelect.addAll(fieldsList);
            return this;
        }

        public Builder whereCondition(Condition condition){
            this.whereCondition.addCondition(condition);
            return this;
        }

        public Builder orderBy(SortOrder sortOrder){
            this.sortOrder.add(sortOrder);
            return this;
        }

        public Builder offset(Integer offset){
            this.offset = offset;
            return this;
        }

        public Builder limits(Integer limits){
            this.limits = limits;
            return this;
        }

    }

    public class SortOrder{
        private String fieldName;
        public SortDirection direction;

        public SortOrder(String fieldName){
            this.fieldName = fieldName;
        }

        public SortOrder(String fieldName, SortDirection direction){
            this(fieldName);
            this.direction = direction;
        }

        public String toSortOrderString(){
            List<String> sortOrderStrings = new List<String>{
                fieldName
            };
            if(direction != null){
                switch on direction {
                    when ASCENDING {
                        sortOrderStrings.add(' ASC');
                    }
                    when DESCENDING {
                        sortOrderStrings.add('DESC');
                    }
                }
            }

            return String.join(sortOrderStrings, ' ');
        }
    }

    public class Condition {
        private fdtn_SOQL.LogicalOperator logicalOperator;
        private List<Expression> expressions;

        public Condition(){
            this(fdtn_SOQL.LogicalOperator.AND_VALUE);
        }
        public Condition(fdtn_SOQL.LogicalOperator logicalOperator){
            this.logicalOperator = logicalOperator;
            this.expressions = new List<Expression>();
        }

        public Condition addCondition(Condition condition){
            this.expressions.add(new ConditionExpression(condition));
            return this;
        }

        public Condition equals(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' = ', value));
            return this;
        }

        public Condition notEquals(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' != ', value));
            return this;
        }

        public Condition lessThan(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' < ', value));
            return this;
        }

        public Condition lessOrEqual(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' <= ', value));
            return this;
        }

        public Condition greaterThan(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' > ', value));
            return this;
        }

        public Condition greaterOrEqual(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' >= ', value));
            return this;
        }

        public Condition isLike(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' LIKE ', value));
            return this;
        }

        public Condition isIn(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' IN ', value));
            return this;
        }

        public Condition isNotIn(String fieldName, Object value){
            this.expressions.add(new FieldExpression(fieldName, ' NOT IN ', value));
            return this;
        }

        public String toConditionString(){
            List<String> expressionString = new List<String>();
            for (Expression condExp : this.expressions) {
                expressionString.add(condExp.toExpressionString());
            }

            switch on logicalOperator {
                when AND_VALUE {
                    return String.join(expressionString, ' AND ');
                }
                when OR_VALUE {
                    return String.join(expressionString, ' OR ');
                }
            }

            return ' ';
        }

        public override String toString(){
            return JSON.serialize(this);
        }
    }

    public class ConditionExpression implements Expression {
        private Condition condition;

        private ConditionExpression(Condition condition){
            this.condition = condition;
        }

        public String toExpressionString(){
            return '(' + condition.toConditionString() + ')';
        }

        public override String toString(){
            return JSON.serialize(this);
        }
    }

    public class FieldExpression implements Expression{
        private String fieldName;
        private String operator;
        private Object value;

        public FieldExpression(String fieldName, String operator, Object value){
            this.fieldName = fieldName;
            this.operator = operator;
            this.value = value;
        }

        public String toExpressionString(){
            return this.fieldName + ' ' + this.operator + this.formatValue();
        }

        private String formatValue(){
            if(this.value instanceof String){
                return '\'' + String.escapeSingleQuotes(String.valueOf(this.value)) + '\'';
            }
            else if(this.value instanceof Integer || this.value instanceof Long
                    || this.value instanceof Decimal || this.value instanceof Double
                    || this.value instanceof Boolean){
                return String.valueOf(this.value);
            }
            else if (this.value instanceof Datetime) {
                Datetime dt = (Datetime)this.value;
                return dt.format('yyyy-MM-dd hh:mm:ss','Asia/Hong kong');
            }
            else if(this.value instanceof List<String>){
                return '(' + String.join(convertToStringList((List<String>)this.value), ', ') + ')';
            }
            return ' NULL ';
        }

        private List<String> convertToStringList(List<String> values){
            List<String> stringList = new List<String>();
            for (String str : values) {
                stringList.add('\'' + String.escapeSingleQuotes(str) + '\'');
            }
            return stringList;
        }

        private List<String> convertToStringList(List<Long>values) {
            List<String> longList = new List<String>();
            for (Long value: values) {
                longList.add(String.valueOf(value));
            }
            return longList;
        }

        public override String toString(){
            return JSON.serialize(this);
        }
    }

    private interface Expression {
        String toExpressionString();
    }
}