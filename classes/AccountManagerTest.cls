@IsTest
public class AccountManagerTest {
	@isTest static void testGetAccount() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri ='https://yourInstance.my.salesforce.com/services/apexrest/Accounts/'+ recordId +'/contacts';
        system.debug('Uri'+request.requestUri);
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        Account thisAccount = AccountManager.getAccount();
        // Verify results
        System.assert(thisAccount != null);
        System.assertEquals('Test record', thisAccount.Name);
    }
    
    static Id createTestRecord() {
        // Create test record
        Account accountTest = new Account(
            Name='Test record');
        insert accountTest;
        
        Contact contactTest = new Contact(
            AccountId=accountTest.Id,
            LastName = 'Test Contact');
        insert contactTest;
        system.debug('accountTest'+accountTest.Id);
        return accountTest.Id;
    }          
}