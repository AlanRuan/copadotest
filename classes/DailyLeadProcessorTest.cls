@isTest
private class DailyLeadProcessorTest {
    // Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void testScheduledJob() {
        // Create some out of date Opportunity records
        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<200; i++) {
            Lead l = new Lead(
                FirstName = 'Lead ' + i,
                LastName ='Test',
                Company = 'Test Company'
            );
            leads.add(l);
        }
        insert leads;

        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP,
            new DailyLeadProcessor());
        // Verify the scheduled job has not run yet.
        List<Lead> l = [SELECT Id
            FROM Lead
            WHERE LeadSource='Dreamforce'];
        System.assertEquals(0, l.size(), 'lead was updated before schedule job');
        // Stopping the test will run the job synchronously
        Test.stopTest();
        // Now that the scheduled job has executed,
        // check that our tasks were created
        l = [SELECT Id
            FROM Lead
            WHERE LeadSource='Dreamforce'];
        System.assertEquals(200, l.size(), 'lead was NOT updated after schedule job');
    }
}