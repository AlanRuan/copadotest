/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class StringFlow {
    global StringFlow() {

    }
    @InvocableMethod(label='Split Multiple Values' description='Split multiple values from a checkbox group or a multi-select picklist into a collection.')
    global static List<List<String>> splitMultipleValues(List<String> inputList) {
        return null;
    }
}
