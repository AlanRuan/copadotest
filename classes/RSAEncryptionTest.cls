@isTest
public without sharing class RSAEncryptionTest {
    @isTest
    public static void Test()
    {
        //generate private key
        //openssl command: openssl genrsa -des3 -out private.pem 2048
        
        //openssl command: openssl rsa -in private.pem -noout -text
        //public key modulus (remove colons, remove 00 prefix & make into single line)
        //public key exp 0x10001 = 10001
        RSAEncyption.PublicKey pub = new RSAEncyption.PublicKey('f39dd44cceb6b25679e9318cabc7a679a7cd6caf5a96df59e52f74cc8f3d727a8c05d8a9067a700a93a728433bd1de65903d5ea2909761e06ff60bb980b5aaf48b3499d5b544448335e35474390f4873d04bb42f417e38fac333a5e7ffea5f67f4a9bc1df0a34a8b99c520a61711ffa9ab525c73ca13d1b9f44e35cc8bb7339bccd3a90115cffbc11349b216e7411d0f8d0f4770e3b77eaa8d62bb12f1ded9147d04f2aea39f09987b116cc47d996409718944e7f31dc416857eff79f9b2dd2c774fb70f9e0bea34b1538ee53a55dd92ee0dfa6e964112d6c4ff3770a44ecf06695932e6168ef7a64fa320a9d26f50b9b6109acfe148c74c40d37463719eef17'                                       
                                              ,'10001');    
                                              
        RSAEncyption r = new RSAEncyption(pub);
        
        String message = 'hello world hello world hello world hello world';
        String encrypted = r.encrypt(message);
        
        System.debug('Original Message: ' + message);
        System.debug('Encrypted Message: ' + encrypted);
        
        //openssl command: openssl rsa -in private.pem -noout -text
        //private key modulus (remove colons, remove 00 prefix & make into single line), same value as public key mod
        //private key privateExponent (remove colons & make into single line)
        RSAEncyption.PrivateKey pvt = new RSAEncyption.PrivateKey('f39dd44cceb6b25679e9318cabc7a679a7cd6caf5a96df59e52f74cc8f3d727a8c05d8a9067a700a93a728433bd1de65903d5ea2909761e06ff60bb980b5aaf48b3499d5b544448335e35474390f4873d04bb42f417e38fac333a5e7ffea5f67f4a9bc1df0a34a8b99c520a61711ffa9ab525c73ca13d1b9f44e35cc8bb7339bccd3a90115cffbc11349b216e7411d0f8d0f4770e3b77eaa8d62bb12f1ded9147d04f2aea39f09987b116cc47d996409718944e7f31dc416857eff79f9b2dd2c774fb70f9e0bea34b1538ee53a55dd92ee0dfa6e964112d6c4ff3770a44ecf06695932e6168ef7a64fa320a9d26f50b9b6109acfe148c74c40d37463719eef17'
                                               ,'00f2d322c5dc55a6b5239718d88a70dab2f05b8635d32a073ee77ec20113d5bfc1fec7e509b5775d2e6db6741f7004e4947f8d6c42c5b4dece834ad0acfa6a1a18de9873addc9c4b5e2ddc8655c27a45518b11aa6c5fef9c83f706081c93addda314f00a9e1d39e617f811d1553c31a8904a4031ff0831711ed5310fd6ee7c91668c6bfa61b1468cdaafb363dc6b9d529877499d067aa2a3d57435710a21543033925d403dc0b94ba603ea077036bd2422c63be75ec85f8be0b027be01c760f6a8d7a568fef22918b8a1d81cce012eb93a9d0ddf915c4749b6c5c9b58699001cecde0600414bf232c26c894377a359d97566c21f5e5f5783c43dd7fb170bf80c81');

        RSAEncyption r2 = new RSAEncyption(pvt);
        
        try
        {    
            String decrypted = r2.decrypt(encrypted );
            System.debug('Decrypted Message: ' + decrypted);
        }
        catch(Exception ex)
        {
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString()); 
        }
    }
}