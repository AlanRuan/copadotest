/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class JoinTextCollectionAction {
    global JoinTextCollectionAction() {

    }
    @InvocableMethod(label='Join Text Collection' description='Concatenate a list of text values with optional delimiters and enclosures.')
    global static List<String> execute(List<Illest.JoinTextCollectionAction.Input> inputList) {
        return null;
    }
    global static String formatRegExp(String naive) {
        return null;
    }
global class Input {
    @InvocableVariable(label='Delimiter' required=false)
    global String delimiter;
    @InvocableVariable(label='Enclosure' description='Wrap this enclosure around each value.' required=false)
    global String enclosure;
    @InvocableVariable(label='Escape Character' required=false)
    global String escapeCharacter;
    @InvocableVariable(label='Text Collection' required=false)
    global List<String> valueList;
    global Input() {

    }
    global String getEscapedEnclosure() {
        return null;
    }
}
}
