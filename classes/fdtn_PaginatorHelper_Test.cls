@isTest
private  class fdtn_PaginatorHelper_Test {
    public class TestException extends Exception {}

    @testSetup
    static void setData(){
        Account acc = new Account();
        acc.Name = 'Collin Feng';
        acc.NumberOfEmployees = 12345678;
        insert acc;
    }
    
    @isTest
    static void testGetPaginatorInfo(){
        fdtn_SOQL fdtnSoql = new fdtn_SOQL.Builder('Account')
                                .addField('Name, Type, CreatedDate')
                                .orderBy(new fdtn_SOQL.SortOrder('Name', fdtn_SOQL.SortDirection.DESCENDING))
                                .build();
        fdtn_PaginatorEntity.fdtn_PaginatorResponse fdtnPaginatorResponse = new fdtn_PaginatorHelper(fdtnSoql, 10, 1).getPaginatorInfo();
        
        System.assertEquals(1, fdtnPaginatorResponse.totalPageNo);
    }
}