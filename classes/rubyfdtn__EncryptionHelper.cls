/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EncryptionHelper {
    global EncryptionHelper() {

    }
    global static String decrypt(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String encryptedText) {
        return null;
    }
    global static String decrypt(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String encryptedText, Blob initializationVector) {
        return null;
    }
    global static String encrypt(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String plainText) {
        return null;
    }
    global static String generateHash(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String inputString) {
        return null;
    }
    global static String getInitializationVector(rubyfdtn.EncryptionHelper.RubySystem rubySystem) {
        return null;
    }
    global static String signWithKey(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String plainText) {
        return null;
    }
    global static Boolean verifyWithKey(rubyfdtn.EncryptionHelper.RubySystem rubySystem, String plainText, String signatureText) {
        return null;
    }
global enum RubySystem {CIM, OnePaymentPlatformIntegration1, OnePaymentPlatformIntegration2, TAPGO}
}
