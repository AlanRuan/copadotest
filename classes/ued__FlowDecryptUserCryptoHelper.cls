/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FlowDecryptUserCryptoHelper {
    global FlowDecryptUserCryptoHelper() {

    }
    @InvocableMethod(label='Decrypt Token' description='Pass a token to Decrypt.')
    global static List<ued.FlowDecryptUserCryptoHelper.UEDResult> doDecrypt(List<ued.FlowDecryptUserCryptoHelper.UEDRequest> decryptionRequest) {
        return null;
    }
global class UEDRequest {
    @InvocableVariable( required=true)
    global String encryptedToken;
    @InvocableVariable( required=false)
    global Boolean urlDecode;
    @InvocableVariable( required=false)
    global String urlEncodingScheme;
    global UEDRequest() {

    }
}
global class UEDResult {
    @InvocableVariable( required=false)
    global String decryptedToken;
    @InvocableVariable( required=false)
    global String error;
    global UEDResult() {

    }
}
}
