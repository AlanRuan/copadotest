public inherited sharing class fdtn_PaginatorHelper {

    public Integer pageSize;
    public Integer currentPageNo;
    public fdtn_PaginatorEntity.fdtn_PaginatorResponse paginatorResponse;
    public fdtn_SOQL fdtnSoql;

    public fdtn_PaginatorHelper(){}

    public fdtn_PaginatorHelper(fdtn_SOQL fdtnSoql, Integer pageSize, Integer currentPageNo){
        this.fdtnSoql = fdtnSoql;
        this.pageSize = pageSize;
        this.currentPageNo = currentPageNo;
        this.paginatorResponse = new fdtn_PaginatorEntity.fdtn_PaginatorResponse();
    }

    public fdtn_PaginatorEntity.fdtn_PaginatorResponse getPaginatorInfo(){
        Integer recordCount = this.getRecordCount();
        this.paginatorResponse.recordCount = recordCount;

        Decimal dec = Decimal.valueOf(recordCount)/this.pageSize;
        Integer totalPageNo =  Integer.valueOf(dec.round(System.RoundingMode.CEILING));
        this.paginatorResponse.totalPageNo = totalPageNo;

        this.currentPageNo = totalPageNo < this.currentPageNo ? totalPageNo : this.currentPageNo;
        this.paginatorResponse.currentPageNo = this.currentPageNo;
        this.paginatorResponse.pageSize = this.pageSize;

        Integer offset = (this.currentPageNo - 1) * this.pageSize;

        String recordQuerySOQL = this.fdtnSoql.getQueryString() + ' limit ' + this.pageSize + ' offset ' + offset;
        List<SObject> sObj =  Database.query(recordQuerySOQL);
        this.paginatorResponse.records = sObj;

        return this.paginatorResponse;
    }

    private Integer getRecordCount(){
        return this.fdtnSoql.getRecordCount();
    }

}