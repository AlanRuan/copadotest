public class AddPrimaryContact implements Queueable{
    private Contact contact;
    private String state;
    public AddPrimaryContact(Contact inputContact, String inputState) {
        this.contact = inputContact;
        this.state = inputState;
    }
    public void execute(QueueableContext context) {
        
        List<Account> accounts = [SELECT Id,Name from Account where BillingState = :state LIMIT 200];
        List<Contact> contactToUpdate = new List<Contact>();
        for (Account account : accounts) {
			Contact contactClone = contact.clone();
            contactClone.AccountId=account.id;
                contactToUpdate.add(contactClone);

            
            // perform other processing or callout
        }
        insert contactToUpdate;
    }
}