public class CaseService {
public static void closeCases (Set<Id> caseIds, String closeReason) {
        // Validate parameters
        if(caseIds==null || caseIds.size()==0)
            throw new CaseServiceException('Cases not specified.');
        if(closeReason==null)
            throw new CaseServiceException('Close reason not specified.');
        // Query Cases
        List<Case> Cases =
            [select Id,Description from Case where Id in :caseIds];
        // Update Cases and Lines (if present)
        List<Case> casesToUpdate = new List<Case>();
        for(Case caseRecord : Cases) {
                caseRecord.Description = closeReason;
            	caseRecord.Reason = closeReason;
            	caseRecord.Status = 'Closed';
                casesToUpdate.add(caseRecord);
            }

        // Update the database
        SavePoint sp = Database.setSavePoint();
        try {
            update casesToUpdate;
        } catch (Exception e) {
            // Rollback
            Database.rollback(sp);
            // Throw exception on to caller
            throw e;
        }
    }
    public class CaseServiceException extends Exception {} 
}