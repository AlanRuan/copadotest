public class DailyLeadProcessor implements Schedulable {
    public void execute(SchedulableContext ctx) {
        List<Lead> leads = [SELECT Id, Name, LeadSource
            FROM Lead
            WHERE LeadSource ='' LIMIT 200];
        List<Lead> leadToUpdate = new List<Lead>();
        for (Lead l: leads){
            l.LeadSource='Dreamforce';
            leadToUpdate.add(l);
        }
        update leadToUpdate;
    }
}