@isTest
public class AddPrimaryContactTest {
    
    @isTest
    private static void testQueueableClass() {
                List<Account> accounts = new List<Account>();
        // add a parent account
        accounts.add(new Account(name='Parent'));
        // add 100 child accounts
        for (Integer i = 0; i < 500; i++) {
            if (i<250){
                accounts.add(new Account(
                name='Test Account'+i, BillingState='NY'
            ));
            }
            else{
                accounts.add(new Account(
                name='Test Account'+i, BillingState='CA'
            ));
            }
            
        }
        insert accounts;
        
        Contact contact = new Contact(FirstName='Simon',LastName='Connock');
        insert contact;

        Test.startTest();
        Id jobId = System.enqueueJob(new AddPrimaryContact(contact,'CA'));
        Test.stopTest();
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Contact.Account.BillingState='CA'];
        System.assertEquals(200, contacts.size(),'ERROR');
    }
}