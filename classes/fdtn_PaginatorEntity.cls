public inherited sharing class fdtn_PaginatorEntity {
    /* public class fdtn_PaginatorRequest{
        public Integer currentPageNo { get; set; }
        public Integer pageSize { get; set; }
        public String sortOrder {get;set;}
    } */

    public class fdtn_PaginatorResponse{
        @AuraEnabled public Integer currentPageNo { get; set; }
        @AuraEnabled public Integer pageSize { get; set; }
        @AuraEnabled public Integer totalPageNo { get; set; }
        @AuraEnabled public Integer recordCount { get; set; }
        /* @AuraEnabled public Boolean hasNext { get; set; }
        @AuraEnabled public Boolean hasPrevious { get; set; }
        @AuraEnabled public Integer nextPageNo { get; set; }
        @AuraEnabled public Integer previousPageNo { get; set; }
        @AuraEnabled public Integer firstPageNo { get; set; }
        @AuraEnabled public Integer lastPageNo { get; set; }
        @AuraEnabled public String sortOrder {get;set;} */
        @AuraEnabled public List<SObject> records { get; set; }

        public fdtn_PaginatorResponse(){
        }
    }
}