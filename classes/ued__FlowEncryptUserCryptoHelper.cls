/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FlowEncryptUserCryptoHelper {
    global FlowEncryptUserCryptoHelper() {

    }
    @InvocableMethod(label='Encrypt Token' description='Pass a token to encrypt.')
    global static List<ued.FlowEncryptUserCryptoHelper.UEDResult> doEncrypt(List<ued.FlowEncryptUserCryptoHelper.UEDRequest> encryptionRequest) {
        return null;
    }
global class UEDRequest {
    @InvocableVariable( required=false)
    global String encryptForUserId;
    @InvocableVariable( required=true)
    global String token;
    @InvocableVariable( required=false)
    global Boolean urlEncode;
    @InvocableVariable( required=false)
    global String urlEncodingScheme;
    global UEDRequest() {

    }
}
global class UEDResult {
    @InvocableVariable( required=false)
    global String encryptedToken;
    @InvocableVariable( required=false)
    global String error;
    global UEDResult() {

    }
}
}
