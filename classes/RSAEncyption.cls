// POC in progress, do not deploy

public without sharing class RSAEncyption {

    private Key key;
    // Hex digits
    private static final String DIGITS = '0123456789abcdef';
    private static final Decimal HEX_BASE = 16;

    public abstract class Key {
        private String modulus;
        public Key(String modulus) {
            this.modulus = modulus;
        }
    }
    
    public virtual class PublicKey extends Key {
        private String exponent;
        public PublicKey(String modulus, String exponent) {
            super(modulus);
            this.exponent = exponent;
        }
    }
    
    public class PrivateKey extends PublicKey {
        private String privateExponent;
        public PrivateKey(String modulus, String privateExponent) {
            super(modulus, null);
            this.privateExponent = privateExponent;
        }
        
        public PrivateKey(String modulus, String exponent, String privateExponent) {
            super(modulus, exponent);
            this.privateExponent = privateExponent;
        }
    }
    
    public RSAEncyption(Key key) {
        this.key = key;
    }
    
    public String encrypt(String input) {
        PublicKey publicKey = (PublicKey)this.key;
        return modPowEncrypt(input, publicKey.modulus, publicKey.exponent);
    }
    
    public String decrypt(String input) {
        PrivateKey privateKey = (PrivateKey)this.key;
        return modPowDecrypt(input, privateKey.modulus, privateKey.privateExponent);
    }
    
    public String modPowEncrypt(String input, String modulus, String exponent) {
        
        //Blob mod = EncodingUtil.base64Decode(modulus);
        //Blob exp = EncodingUtil.base64Decode(exponent);
        
        Blob mod = Blob.valueOf(modulus);
        Blob exp = Blob.valueOf(exponent);
        
       	System.debug('mod ' + hexToDecimal(EncodingUtil.convertToHex(mod)) );
        System.debug('exp ' + hexToDecimal(EncodingUtil.convertToHex(exp)) );
        System.debug(exp.toString());

        //Blob pn = Blob.valueOf(input);
        Blob pn = Blob.valueOf(String.fromCharArray(pkcs1Pad2(input, mod.size() - 1)));
        // Pad password.nonce
        String temp = String.fromCharArray(pkcs1Pad2(input, mod.size()/2-1));
        
        //System.debug(pn.toString());
        //System.debug(mod.size());

        //Decimal modDec = hexToDecimal(EncodingUtil.convertToHex(mod));
        //Decimal expDec = hexToDecimal(EncodingUtil.convertToHex(exp));
        Decimal pnDec = hexToDecimal(EncodingUtil.convertToHex(pn));
        
        Decimal modDec = hexToDecimal(modulus);
        Decimal expDec = hexToDecimal(exponent);
        
        //System.debug(modDec.toPlainString());
        //System.debug(expDec.toPlainString());
        //System.debug(pnDec.toPlainString());

        // Calcluate padded^exp % mod and convert to hex
        Decimal result = modPow(pnDec, expDec, modDec);
        String hexResult = decimalToHex(result);
        // If length is uneven, add an extra 0
        if ((hexResult.length() & 1) == 1) {
            hexResult = '0' + hexResult;
        }

        //System.debug(hexResult);
        
        // Generate the data to be encrypted.
        Blob encodedData = EncodingUtil.convertFromHex(hexResult);
        
         return EncodingUtil.base64Encode(encodedData);
        //return EncodingUtil.urlEncode(EncodingUtil.base64Encode(encodedData),'UTF-8');
    }
    
    public String modPowDecrypt(String input, String modulus, String exponent ) {
        
        //Blob mod = EncodingUtil.base64Decode(modulus);
        //Blob exp = EncodingUtil.base64Decode(exponent);
        
        Blob mod = Blob.valueOf(modulus);
        Blob exp = Blob.valueOf(exponent);
        
       	//System.debug('mod ' + hexToDecimal(EncodingUtil.convertToHex(mod)) );
        //System.debug('exp ' + hexToDecimal(EncodingUtil.convertToHex(exp)) );
        //System.debug(exp.toString());
        
        Blob pn = EncodingUtil.base64Decode(input);
        
		//System.debug(pn.size());
        //System.debug(pn.toString());

        //Decimal modDec = hexToDecimal(EncodingUtil.convertToHex(mod));
        //Decimal expDec = hexToDecimal(EncodingUtil.convertToHex(exp));
        Decimal pnDec = hexToDecimal(EncodingUtil.convertToHex(pn));
        
        Decimal modDec = hexToDecimal(modulus);
        Decimal expDec = hexToDecimal(exponent);
        
        System.debug('mod ' + modDec.toPlainString());
        System.debug('exp ' + expDec.toPlainString());
        
        //System.debug(modDec.toPlainString());
        //System.debug(expDec.toPlainString());
        //System.debug(pnDec.toPlainString());

        // Calcluate padded^exp % mod and convert to hex
        Decimal result = modPow(pnDec, expDec, modDec);
        String hexResult = decimalToHex(result);
        // If length is uneven, add an extra 0
        if ((hexResult.length() & 1) == 1) {
            hexResult = '0' + hexResult;
        }

        String temp = blobToString(hexResult, 'UTF-8');

        return temp;
    }
    
    public static String blobToString(String hex, String inCharset)
    {
        //String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        
        for(Integer i = bytesCount-1; i >= 0; i--)
        {
            //System.debug(bytes[i]);
            
            if (bytes[i].equals('00'))
            {
                //System.debug('padding indicator');
                
                List<String> bytesTemp = new List<String>();
                
                for(Integer j = i; j < bytesCount; j++)
                {
                    bytesTemp.add(bytes[j]);
                }
                
                bytes = bytesTemp;
                
                break;
            }
        }
        
        //System.debug('%' + String.join(bytes, '%'));
        
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
	}

    @testVisible
    private static Decimal hexToDecimal(String hex) {
        Decimal result = 0;
        integer length = hex.length();
        integer i = 0;
        while(i < length) {
            integer hexByte = DIGITS.indexOf(hex.substring(i, i + 1).toLowerCase());
            i++;
            result += hexByte * HEX_BASE.pow(length - i);
        }
        return result;
    }

    @testVisible
    private static String decimalToHex(Decimal d) {
        String hex = '';
        while (d > 0) {
            Decimal digit = modulus(d, HEX_BASE); // rightmost digit
            hex = DIGITS.substring(digit.intValue(), digit.intValue() + 1) + hex; // string concatenation
            d = d.divide(16, 0, RoundingMode.FLOOR);
        }
        return hex;
    }

    // base^exp % mod
    @testVisible
    private static Decimal modPow(Decimal base, Decimal exp, Decimal mod) {
        if (base < 1 || exp < 0 || mod < 1) {
            return -1;
        }

        Decimal result = 1;
        while (exp > 0) {
           if ((exp.longValue() & 1) == 1) {
               result = modulus((result * base), mod);
           }
           base = modulus((base * base), mod);
           exp = exp.divide(2, 0, RoundingMode.FLOOR);
        }
        return result;
    }

    // dividend % divisor
    @testVisible
    private static Decimal modulus(Decimal dividend, Decimal divisor) {
        Decimal d = dividend.divide(divisor, 0, RoundingMode.FLOOR);
        return dividend - (d * divisor);
    }

    // Pad using PKCS#1 v.2. See https://en.wikipedia.org/wiki/PKCS_1
    // s = String to pad
    // n = bytes to fill must be bigger than s.length()
    @testVisible
    private static List<integer> pkcs1Pad2(String s, integer n) {
        // Byte array
        List<integer> ba = new List<integer>();
        // Fill array with zeros to get the right size
        for(integer i = 0; i < n; i++) {
            ba.add(0);
        }
        integer i = s.length() - 1;
        while(i >= 0 && n > 0) {
            ba.set(--n, s.charAt(i--));
        }
        ba.set(--n, 0);
        while(n > 2) { // random non-zero pad
            // Since the array is converted to a string, choose integers that corresponds
            // to a proper char code see http://www.asciitable.com
            integer rnd = Math.round(Math.random() * (127 - 32) + 32);
            ba.set(--n, rnd);
        }
        ba.set(--n, 2);
        ba.set(--n, 0);
        return ba;
    }
}