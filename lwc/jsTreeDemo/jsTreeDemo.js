import { LightningElement } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
// import jquery & dataTables library from static resource
import jQuery224 from '@salesforce/resourceUrl/jquery';
import jsTree from '@salesforce/resourceUrl/jstree_3_3_12';



export default class JsTreeDemo extends LightningElement {

  renderedCallback() {

    Promise.all([
      loadScript(this, jQuery224),
      loadScript(this, jsTree + '/jstree_3_3_12/jstree.js'),
      loadScript(this, jsTree + '/jstree_3_3_12/jstree.min.js'),
      loadStyle(this, jsTree + '/jstree_3_3_12/themes/default/style.css')
    ])

      .then(() => {
        //const root = this.template.querySelector('.html');
        //root.style.height = this.height + "px";

        //$(root).jstree();
        //$(this.template.querySelector('.html')).jstree();
        //const tree = this.template.querySelector('.treeCls');
        //$('#jstree').jstree();
        //const element = this.template.querySelector('[data-id="html"]');
        //$(element).jsTree();
        //    "plugins" : [ "wholerow", "checkbox" ]
        //  })

        $(this.template.querySelector('.jsTree')).jstree(


          {

            'plugins': ["wholerow", "checkbox"],

            'core': {
              'data': [
                {
                  'text': 'Tablet & Computer',
                  'state': {
                    'opened': false,
                    'selected': true
                  },
                  'children': [

                    //Laptop
                    {
                      'text': 'Laptop',
                      'children': [
                        { 'text': 'Laptop' }
                      ]
                    },

                    //Tablets
                    {
                      'text': 'Tablet',
                      'children': [
                        { 'text': '5G Tablet' },
                        { 'text': 'Tablet' }
                      ]
                    },

                    //Desktops
                    {
                      'text': 'Desktop',
                      'children': [
                        { 'text': 'Tower' },
                        { 'text': 'Micro' },
                        { 'text': 'All-In-One' },
                        { 'text': 'Gaming' },
                        { 'text': 'Other' }
                      ]

                    },



                    //Networking Devices
                    {
                      'text': 'Networking Device',
                      'children': [
                        { 'text': 'Ethernet + SEP Switch' }
                      ]

                    }
                  ]
                }]
            }


          }




        );

      })



  }






}