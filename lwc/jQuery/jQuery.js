import { LightningElement } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import jQuery from '@salesforce/resourceUrl/jquery';
import jstree from '@salesforce/resourceUrl/jstree_3_3_12';
import jqxtree from '@salesforce/resourceUrl/jqwidgets12_1_2';


export default class Jstree extends LightningElement {

    renderedCallback(){
        loadScript(this, jQuery)
        .then(() => {
            console.log('JQuery loaded.');
        })
        .catch(error=>{
            console.log('Failed to load the JQuery : ' +error);
        });

    }

    slideIt(event){
        $(this.template.querySelector('.panel')).slideToggle("slow");
    }

    slideRight(event){
        $(this.template.querySelector('.innerDiv')).animate({left: '275px'});
    }


}