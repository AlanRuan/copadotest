import { LightningElement, wire } from 'lwc';
import jsTree from '@salesforce/resourceUrl/jstree_3_3_12';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import getAllTreeData from '@salesforce/apex/fdtn_AccountController.getAllTreeData';
import jQuery from '@salesforce/resourceUrl/jquery';

export default class JstreePoc extends LightningElement {

  connectedCallback() {

  }
  treeAccountData = [];

  @wire(getAllTreeData)
  wireGetAllTreeData({ error, data }) {
    if (data) {
      this.treeAccountData = data;
      //this.onLoadstaticResorce();
    }
  }

  renderedCallback() {
    console.log('Start to load js........');
    Promise.all([
      loadScript(this, jQuery),
      loadScript(this, jsTree + '/jstree_3_3_12/jstree.js'),
      loadScript(this, jsTree + '/jstree_3_3_12/jstree.min.js'),
      loadStyle(this, jsTree + '/jstree_3_3_12/themes/default/style.css')
    ]).then(() =>{
      let treeAccountEle = this.template.querySelector('.treeAccount');
      treeAccountEle.jstree({
          'core': {
              data: this.treeAccountData
          }
      });
    })
  }
}