import { LightningElement } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import jqxTree from '@salesforce/resourceUrl/jqwidgets12_1_2';


export default class jqwidget extends LightningElement {

    renderedCallback(){

        Promise.all([
            loadScript(this, jqxTree + '/jqwidgets12_1_2/scripts/jquery-1.12.4.min.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxcore.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxbuttons.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxscrollbar.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxpanel.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxtree.js'),
            loadScript(this, jqxTree + '/jqwidgets12_1_2/jqxcheckbox.js'),
            loadStyle(this, jqxTree + '/jqwidgets12_1_2/styles/jqx.base.css')
        ])
        .then(() => {
            console.log('Files loaded.New');
            this.template.querySelector('.jqxTree').jqxTree({ height: '400px', width: '1030px'});
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading jqxtree',
                    message: error.message,
                    variant: 'error'
                })
            );
        });
    }
}