import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import jqxTree1 from '@salesforce/resourceUrl/jqwidgets12_1_2';
import jqxTree from '@salesforce/resourceUrl/jqwidgets';
import jQuery from '@salesforce/resourceUrl/jquery1_12_4';


export default class Jqwidgettree extends LightningElement {
    

    treeInitialized = false;

    renderedCallback(){
        console.log('Start here........');
        if (this.treeInitialized) {
            return;
        }
        this.treeInitialized = true;
        console.log('Start to load js........');
    Promise.all([
        loadScript(this, jQuery),
        //loadScript(this, jqxTree + '/scripts/jquery-1.12.4.min.js')
        loadScript(this, jqxTree + '/jqxcore.js'),
        loadScript(this, jqxTree + '/jqxbuttons.js')
        /*loadScript(this, jqxTree + '/jqxscrollbar.js')
        loadScript(this, jqxTree + '/jqxpanel.js'),
        loadScript(this, jqxTree + '/jqxtree.js'),
        loadScript(this, jqxTree + '/jqxcheckbox.js'),
        loadStyle(this, jqxTree + '/styles/jqx.base.css')*/

        
    ])
        .then(() => {
            console.log('Start to initialize........');
            this.initializeUI();

        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading jqxtree',
                    message: error.message,
                    variant: 'error'
                })
            );
        });
    }

    initializeUI(){
        console.log('Start to initialize UI........');
        this.initializeData();
        const root = this.template.querySelector('.treeGrid');
        root.style.height = this.height + "px";

        dataAdapter = new this.jqx.dataAdapter(this.source);
        root.jqxTreeGrid(
            {
                width: 850,
                theme: "arctic",
                source: this.dataAdapter,
                sortable: true,
                columns: [
                  { text: 'FirstName', dataField: 'FirstName', width: 200 },
                  { text: 'LastName', dataField: 'LastName', width: 120 },
                  { text: 'Title', dataField: 'Title', width: 160 },
                  { text: 'Birth Date', dataField: 'BirthDate', cellsFormat: 'd', width: 120 },
                  { text: 'Hire Date', dataField: 'HireDate', cellsFormat: 'd', width: 120 },
                  { text: 'Address', dataField: 'Address', width: 250 },
                  { text: 'City', dataField: 'City', width: 120 },
                  { text: 'Country', dataField: 'Country' }
                ]
            });
    }

    initializeData(){
        console.log('Start to load data........');
        employees = [
            {
                "EmployeeID": 2, "FirstName": "Andrew", "LastName": "Fuller", "Country": "USA", "Title": "Vice President, Sales", "HireDate": "1992-08-14 00:00:00", "BirthDate": "1952-02-19 00:00:00", "City": "Tacoma", "Address": "908 W. Capital Way", "expanded": "true",
                children: [
                { "EmployeeID": 8, "FirstName": "Laura", "LastName": "Callahan", "Country": "USA", "Title": "Inside Sales Coordinator", "HireDate": "1994-03-05 00:00:00", "BirthDate": "1958-01-09 00:00:00", "City": "Seattle", "Address": "4726 - 11th Ave. N.E." },
                { "EmployeeID": 1, "FirstName": "Nancy", "LastName": "Davolio", "Country": "USA", "Title": "Sales Representative", "HireDate": "1992-05-01 00:00:00", "BirthDate": "1948-12-08 00:00:00", "City": "Seattle", "Address": "507 - 20th Ave. E.Apt. 2A" },
                { "EmployeeID": 3, "FirstName": "Janet", "LastName": "Leverling", "Country": "USA", "Title": "Sales Representative", "HireDate": "1992-04-01 00:00:00", "BirthDate": "1963-08-30 00:00:00", "City": "Kirkland", "Address": "722 Moss Bay Blvd." },
                { "EmployeeID": 4, "FirstName": "Margaret", "LastName": "Peacock", "Country": "USA", "Title": "Sales Representative", "HireDate": "1993-05-03 00:00:00", "BirthDate": "1937-09-19 00:00:00", "City": "Redmond", "Address": "4110 Old Redmond Rd." },
                {
                    "EmployeeID": 5, "FirstName": "Steven", "LastName": "Buchanan", "Country": "UK", "Title": "Sales Manager", "HireDate": "1993-10-17 00:00:00", "BirthDate": "1955-03-04 00:00:00", "City": "London", "Address": "14 Garrett Hill", "expanded": "true",
                    children: [
                           { "EmployeeID": 6, "FirstName": "Michael", "LastName": "Suyama", "Country": "UK", "Title": "Sales Representative", "HireDate": "1993-10-17 00:00:00", "BirthDate": "1963-07-02 00:00:00", "City": "London", "Address": "Coventry House Miner Rd." },
                           { "EmployeeID": 7, "FirstName": "Robert", "LastName": "King", "Country": "UK", "Title": "Sales Representative", "HireDate": "1994-01-02 00:00:00", "BirthDate": "1960-05-29 00:00:00", "City": "London", "Address": "Edgeham Hollow Winchester Way" },
                           { "EmployeeID": 9, "FirstName": "Anne", "LastName": "Dodsworth", "Country": "UK", "Title": "Sales Representative", "HireDate": "1994-11-15 00:00:00", "BirthDate": "1966-01-27 00:00:00", "City": "London", "Address": "7 Houndstooth Rd." }
                    ]
                }
                ]
            }
        ];
        source =
        {
            dataType: "json",
            dataFields: [
                { name: 'EmployeeID', type: 'number' },
                { name: 'FirstName', type: 'string' },
                { name: 'LastName', type: 'string' },
                { name: 'Country', type: 'string' },
                { name: 'City', type: 'string' },
                { name: 'Address', type: 'string' },
                { name: 'Title', type: 'string' },
                { name: 'HireDate', type: 'date' },
                { name: 'children', type: 'array' },
                { name: 'expanded', type: 'bool' },
                { name: 'BirthDate', type: 'date' }
            ],
            hierarchy:
            {
                root: 'children'
            },
            id: 'EmployeeID',
            localData: this.employees
            
        };console.log('Finish to load data........');
    }
}