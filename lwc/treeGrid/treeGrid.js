import {
    LightningElement,
    track,
    api,
    wire
} from 'lwc';
import getTreeGridData from '@salesforce/apex/TreeGrid.getTreeGridData';

export default class TreeGrid extends LightningElement {
    @track columns = [{
            type: 'text',
            fieldName: 'name',
            label: 'name'
        },
        {
            type: 'text',
            fieldName: 'label',
            label: 'label'
        }
    ];
    @track selectedRows = [];
    setSelectedRows(){
             var selectRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();
   
             if(selectRows.length > 0){
                  var tempList = [];
                  var tempListExpanded = [];
                  selectRows.forEach(function (record){
                            tempList.push(record.id);
                  })
                  this.dataObj.forEach(function (record){
                           if(tempList.includes(record.id)){
                               record.items.forEach(function (child){
                                  //tempListExpanded.push(record.id);
                                  tempList.push(child.id);
                               })
                           }
                       })
                  this.selectedRows = tempList;
                  //this.requestExpandRows = tempListExpanded;
                  console.log(this.selectedRows);
             }
         }
     @track treeItems;
     @track error;
     @wire(getTreeGridData)
     wireTreeData({
         error,
         data
     }) {
         if (data) {
             //  alert(data);
             var res = data;
             var tempjson = JSON.parse(JSON.stringify(data).split('items').join('_children'));
             console.log(tempjson);
             this.treeItems = tempjson;
             console.log(JSON.stringify(tempjson, null, '\t'));
         } else {
             this.error = error;
             //  alert('error' + error);
         }
     }
}