import { LightningElement } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import jQuery from '@salesforce/resourceUrl/jquery';
import jstree from '@salesforce/resourceUrl/jstree_3_3_12';
import jqxtree from '@salesforce/resourceUrl/jqwidgets12_1_2';


export default class Jstree extends LightningElement {

    renderedCallback(){
        loadScript(this, jQuery)
        .then(() => {
            console.log('JQuery loaded.');
        })
        .catch(error=>{
            console.log('Failed to load the JQuery : ' +error);
        });
        loadScript(this, jstree)
        .then(() => {
            console.log('jstree_3_3_12 loaded.');
        })
        .catch(error=>{
            console.log('Failed to load the jstree_3_3_12 : ' +error);
        });
        loadScript(this, jqxtree)
        .then(() => {
            console.log('jqxtree loaded.');
        })
        .catch(error=>{
            console.log('Failed to load the jqxtree : ' +error);
        });

        $(this.template.querySelector('.jqxTree')).jqxTree({ height: '400px', width: '330px', hasThreeStates: true, checkboxes: true});
        //$(function () { $('#jstree_demo_div').jstree(); });
    }
/*
    slideIt(event){
        $(this.template.querySelector('.panel')).slideToggle("slow");
    }

    slideRight(event){
        $(this.template.querySelector('.innerDiv')).animate({left: '275px'});
    }

    */
}